<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");

if($_SERVER["REQUEST_METHOD"] === "POST"){
    $json = file_get_contents("php://input");
    $contents = json_decode($json, true);
    sleep(2);
    echo "税抜価格: " . $contents["price"] . " 税込価格: " . $contents["priceWithTax"];
    exit;
}

$list = [
    ["tax"=>0.05, "name"=>"5%"],
    ["tax"=>0.08, "name"=>"8%"],
    ["tax"=>0.10, "name"=>"10%"],
];

echo json_encode($list);
